# -*- coding: utf-8 -*-
"""
Created on Sun Apr 12 10:18:10 2020

@author: PC1
"""

from CMplot import plot_confusion_matrix 
from sklearn.metrics import classification_report, confusion_matrix  
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Dropout
from tensorflow.keras import optimizers
import pandas as pd
import matplotlib.pyplot as plt

import numpy as np
import matplotlib.pyplot as plt
from scipy.io import loadmat
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
#from sklearn.metrics import plot_confusion_matrix


#load data from mat file
data = loadmat('dataarytmiasrdca.mat')

X_data=data["NDATA"]
classes=data["typ_ochorenia"]

#reduce number of classes to two
classes=(classes>1).astype(int)
#split data to training and testing
X_train, X_test, y_train, y_test = train_test_split(X_data, classes, test_size=0.30)



#define model
n_outputs=2
    
model = Sequential()
model.add(Dense(35, input_dim=277, activation='tanh'))
model.add(Dropout(0.86))
model.add(Dense(35, activation='tanh'))
model.add(Dropout(0.86))
model.add(Dense(n_outputs, activation='softmax'))
model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    



#prepare true classification labels for training
y_train=np.hstack((y_train, np.logical_not(y_train)))
#y_test=np.hstack((y_test, np.logical_not(y_test)))
confs=[]
epoch_coef=200
#prepare wrappers for statistics
specificities=np.zeros((10,1))
sensitivities=np.zeros((10,1))
accuracies=np.zeros((10,1))

#train ten times and store statistics 
for i in range(0,1):
    
    
   
    
    

    model.fit(X_train,y_train,epochs=epoch_coef      
                ,batch_size=25,verbose=0)
    
    pred=model.predict_classes(X_test)
    
    target_names=["healthy","sick"]
    
    ev=model.evaluate(X_test,np.hstack((y_test,(y_test<1))))
    print (ev)
   
    
    y_conf=  np.logical_not(y_test)
    conf=confusion_matrix(pred, y_conf,normalize="true")
    plot_confusion_matrix(conf,target_names,normalize=True)
    accuracies[i,0]=ev[1]
    specificities[i,0]=conf[0,0]
    sensitivities[i,0]=conf[1,1]
    confs.append(conf)
    df=pd.DataFrame(data=np.around(np.hstack([accuracies,specificities,sensitivities]),2),columns=['accuracy','specifity','sensitivity'])
    print(classification_report( np.logical_not(y_test), pred, target_names=target_names))
