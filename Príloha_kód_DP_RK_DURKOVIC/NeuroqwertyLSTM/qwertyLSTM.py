
"""
Created on Wed Apr 15 11:04:28 2020

@author: PC1
"""
import os


from keras.callbacks import Callback
from numpy import genfromtxt
import glob
import pandas as pd
from pandas import read_csv
from numpy import dstack
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers import LSTM
from keras.layers import TimeDistributed
from keras.layers import ConvLSTM2D
from keras.utils import to_categorical
from matplotlib import pyplot
import matplotlib.pyplot as plt
import pylab as pl
import numpy as np
import tensorflow as tf
import csv
from keras.models import load_model
from sklearn.metrics import classification_report, confusion_matrix,  ConfusionMatrixDisplay
from keras.backend.tensorflow_backend import set_session
from keras.backend.tensorflow_backend import clear_session
from keras.backend.tensorflow_backend import get_session
import tensorflow
from sklearn.model_selection import train_test_split
from keras.layers.normalization import BatchNormalization



def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True,cmatnum=0):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
    import matplotlib.pyplot as plt
    import numpy as np
    import itertools

    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    plt.tight_layout()
    #plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.savefig("./cmats/cmat" + str(cmatnum))
    # plt.show()
   

class TerminateOnBaseline(Callback):
    """Callback that terminates training when either acc or val_acc reaches a specified baseline
    """
    def __init__(self, monitor='accuracy', baseline=0.9):
        super(TerminateOnBaseline, self).__init__()
        self.monitor = monitor
        self.baseline = baseline

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        acc = logs.get(self.monitor)
        if acc is not None:
            if acc >= self.baseline:
                print('Epoch %d: Reached baseline, terminating training' % (epoch))
                self.model.stop_training = True

#reset keras module if needed
def reset_keras():
 
    tf.keras.backend.clear_session()


#define LSTM/dense model
def def_model():    
    
    
    
        
        
       
        
    
    
        n_outputs=2
        model = Sequential()               
        model.add(LSTM(64, return_sequences=False, activation='tanh', input_shape=(469,3)))
        model.add(Dense(35, activation='tanh'))
        model.add(Dense(n_outputs, activation='softmax'))
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        # model.save('LSTM')
        return model
        
                        
   


#load data
class dataLoader:
    
    @staticmethod
    def load_data(path):
        
        files=glob.glob(path)
        ids=[]
        X_list=[]

        for i,file in enumerate(files):
        
            id_tmp=int(files[i].split('.')[2].split('_')[0])
            ids.append(id_tmp)
            
            datapat=genfromtxt(file, delimiter=',')
            X_list.append(datapat)
            
        x,y=datapat.shape   
        len1=min(len(l) for l in X_list) 
        #crop data to desired length (cannot be lower than shortest sample)
        length=469# 
       
        for  i,data in enumerate(X_list):
            
            
            pad=np.zeros((length,y)) 
            pad[0:len1,:]=data[0:len1,:] 
            X_list[i]=pad[:,1:]
            
       
        X_data=np.stack(X_list,axis=0)    
        
        return ids,X_data
       
        # loads classification targets
    @staticmethod  
    def load_classes (fname,ids):
        
       
            classes=np.zeros(np.asarray(ids).shape)
            class_data = pd.read_csv(fname)
            df=class_data[['pID','gt']]
            df['gt']=df['gt'].astype(int)
            
            dict_classes=df.set_index('pID').to_dict()
            dict_classes=dict_classes['gt']
            
            for i,e in enumerate(ids):
                
                classes[i]=dict_classes.get(e)
            
            classes=classes.astype(int)
            
            
            classesneg=(classes<1).astype(int)
            
            classes=np.vstack((classes,classesneg) ).transpose()
            print('kontrola')
            
            return classes
           




reset_keras()
#load data from one part of database
pathbool=r"./MIT-CS1PD/GT_DataPD_MIT-CS1PD.csv"
path = r"./Concat_DB/1/*"

loader=dataLoader()
ids,X_train=loader.load_data(path)
y_train=loader.load_classes(pathbool,ids)

#load data fom second part of database
pathbool=r"./MIT-CS2PD/GT_DataPD_MIT-CS2PD.csv"
path = r"./Concat_DB/2/*"


ids,X_test=loader.load_data(path)
y_test=loader.load_classes(pathbool,ids)

#staxk data to be able to split them to our liking
classes=np.vstack((y_test,y_train))
X_data=np.vstack((X_test,X_train))

#set desired training part size
X_train, X_test, y_train, y_test = train_test_split(X_data, classes, test_size=0.40)






target_names=["healthy","sick"]

specificities=np.zeros((10,1))
sensitivities=np.zeros((10,1))
accuracies=np.zeros((10,1))
          
#train model ten times and store statistics                   
for x in range(0,10):

    model=def_model()
    
    
    callback = [TerminateOnBaseline(monitor='accuracy', baseline=0.84)]
    model.fit(X_train,y_train,epochs=1200,batch_size=81,verbose=1,callbacks=callback)
    
    
    
    
    print(model.evaluate(X_test,y_test))
    
    
    pred=model.predict_classes(X_test)
    predicted_class_indices=np.argmax(pred)
    conf=confusion_matrix(pred, y_test[:,1],normalize="true")
        # model=load_model("./trainings/qwerty_LSTM" + str(x))
    ev=model.evaluate(X_test,y_test)
    accuracies[x,0]=ev[1]
    pred=model.predict_classes(X_test)
    
    cm=confusion_matrix(pred, y_test[:,1])
    clrep=(classification_report(pred, y_test[:,1], target_names=target_names))
    cm=confusion_matrix(pred, y_test[:,1],normalize='true')
    specificities[x,0]=cm[0,0]
    sensitivities[x,0]=cm[1,1]
    target_names=["healthy","sick"]
  
   
    plot_confusion_matrix(cm,target_names,title='Confusion_matrix',normalize=True,cmatnum=x)

    
    
    # model.save("./trainings/qwerty_LSTM"+ str(x))
    




df=pd.DataFrame(data=np.around(np.hstack([accuracies,specificities,sensitivities]),2),columns=['accuracy','specifity','sensitivity'])


print('hey')




