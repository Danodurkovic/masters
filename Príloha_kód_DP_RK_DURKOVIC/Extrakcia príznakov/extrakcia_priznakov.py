# -*- coding: utf-8 -*-
"""
Created on Tue May 12 19:10:05 2020

@author: PC1
"""
import glob
import numpy as np
import os
import scipy
from scipy import interpolate
from scipy import signal 
import matplotlib.pyplot as plt
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
from scipy.signal import savgol_filter as savgol
from statsmodels.nonparametric.smoothers_lowess import lowess
import itertools



class Attribute_finder:
    
    #Class capable of extrating features from Motion Leap captured data#
    ####################################################################
    #Methods are described near  their definition
    
    def __init__(self, path):
    #constructor, prepare variables and some basic signal preparation before extracting
       
       self.amp=[]
       self.time=[]
       self.ampinter=[]
       self.timeinter=[]
       self.derivative=[] #derivative of amplitude on time domain
       self.closing_velocity=[]
       self.opening_velocity=[]
       self.acceleration=[]
       self.inflex_points=[]
       self.der_mins_ind=[]
       self.der_peaks_ind=[]
       self.sample_time=sample_time=5
       
        
       files=glob.glob(path)
      
       for self.num,file in enumerate(files): #load file you want extract data from 
            
            filenum=1
            
            data=np.genfromtxt(file, delimiter=',')
            print('hey')
            if (self.num==filenum):
               break
            
       self.amp=data[:,0]
       self.time=data[:,1]      
       
       #interpolate data with 5ms step 
       self.timeinter=np.arange(self.time[0],self.time[-1],sample_time)
       self.ampinter=np.interp(self.timeinter,self.time,self.amp)
       #Filter signal with Savitzky-Golay filter
       self.filtered=filtered=savgol(self.ampinter,window_length=15,polyorder=5,axis=0)
       # plt.figure()
       #compute derivative of signal
       self.derivative=np.divide(np.diff(filtered),sample_time)
       self.acceleration=np.diff(self.derivative)
       # plt.plot(self.timeinter,self.derivative, color='red')
       # plt.show
       self.closing_velocity=np.zeros(len(self.derivative))
       self.opening_velocity=np.zeros(np.shape(self.closing_velocity))
       self.op_vel_time=np.copy(self.opening_velocity)
       self.cl_vel_time=np.copy(self.closing_velocity)
        
       for i,e in enumerate(self.derivative): #store opening and closing derivatives
            
            if (e<0):
                
               
                self.closing_velocity[i]=self.derivative[i]
                self.cl_vel_time[i]=self.timeinter[i]
            else:
               
                self.opening_velocity[i]=self.derivative[i]
                self.op_vel_time[i]=self.timeinter[i]
       
        
            
        # returns lenghts of sequences with true value in boolean array
    def runs_of_ones(self,bits):
        return [sum(g) for b, g in itertools.groupby(bits) if b]
       
 
    def plot_extremes(self,der=0,ampinter=None,timeinter=None,co=None,plot=1,inf=0):
        #Plots extremes of given data if plot set to 1, also plots inflex points on original signal if inf=1
        
      
        
        #if arguments not set, set default values
        
        if(not co):co='red'
        if(type(ampinter) is not np.ndarray): ampinter=self.ampinter
        if(type(timeinter) is not np.ndarray):timeinter=self.timeinter
            
        wid=100
        
        #set different obtions for computing original signal peaks
        #and for computing derivative peaks
        if (der==1):
            
           
            hgt1=4/self.sample_time
            hgt2=3/self.sample_time
            wid1=0
            dis1=6
            dis2=19
            
            prom2=1
            prom1=1
        else:
            hgt1=30
            hgt2=-40
            wid1=0
            dis1=16
            dis2=20
            # thresh=15
            prom2=18
            prom1=20
        
       
        # Find extrema of signal
        peaks_ind=signal.find_peaks(ampinter, width=wid1,height=hgt1,distance=dis1,prominence=prom1)[0]
        peaks=ampinter[peaks_ind]
        peaks_time=timeinter[peaks_ind]
        tmp=np.dot(ampinter,-1)
        mins_ind=signal.find_peaks(np.dot(ampinter,-1)+0.16, height=hgt2,distance=dis2,prominence=prom2,plateau_size=1)[0]
        mins=ampinter[mins_ind]
        mins_time=timeinter[mins_ind]
        
        
        #crop extrema numbers to match (important later on when computing periods and other features)
        if (len(mins)>len(peaks)):
            
                mins=mins[0:len(peaks)]
                mins_time=mins_time[0:len(peaks)]
                mins_ind=mins_ind[0:len(peaks)]
        else:
             peaks=peaks[0:len(mins)]
             peaks_time=peaks_time[0:len(mins)]
             peaks_ind=peaks_ind[0:len(mins)]

      
        #verify plot setting, then plot given data, describe axes depending on der setting
        if(plot==1):
            #mark axes for original data
            ax=plt.subplot()
            
            plt.plot(timeinter,ampinter, color=co)
            plt.plot(peaks_time,peaks, color='blue', marker='*', linestyle='none')
            plt.plot(mins_time,mins, color='blue', marker='*', linestyle='none')
            if(der!=1): #set correct axis labels
               
                ax.set_xlabel('cas [ms]')
                ax.set_ylabel('amplituda pohybu [mm]')
                # plt.savefig('signal_data' + str(self.num))
            #mark axes for derivative data
            else:  
                ax.set_ylabel('rychlost pohybu [mm/s]')
                ax.set_xlabel('cas [ms]') 
                plt.savefig('./graphs/derivation' + str(self.num))
            
            #plot inflex points on original data if set to 1 and store them
            if(inf==1):   
                #inf points
                 ax=plt.subplot()
                 mins=self.ampinter[mins_ind]
                 peaks=self.ampinter[peaks_ind]
                 mins_ind,peaks_ind= self.plot_extremes(ampinter=self.derivative, timeinter= self.timeinter[0:-1],der=1,plot=0)
           
                 self.der_mins_ind=mins_ind
                 self.der_peaks_ind= peaks_ind
                 
                 
                 tmp=np.transpose(np.vstack((mins,peaks)))
                 avr=np.average(tmp,axis=1)
                 
                 inf=np.hstack((self.der_mins_ind,self.der_peaks_ind)) 
                 amps=self.ampinter[inf]
                 tim=self.timeinter[inf]
                 sort=np.vstack((tim,amps))
                 sort=sort[sort[:,0].argsort()]
                  
                 # sort= np.transpose(sort).tolist()
                 
                
                 self.inflex_points=sort
                 
                
                 ax.plot(self.inflex_points[1,:],self.inflex_points[0,:],linestyle='none', marker='o',fillstyle='none')
                 plt.savefig('./graphs/signal_data' + str(self.num))
            #save figure for use in documents
        
        return mins_ind,peaks_ind
    
        
    def plot_max_graph(self): # plot graph of maximal amplitudes 
      plt.figure()
      ax = plt.subplot()
      ax.set_xlabel('cas [ms]')
      ax.set_ylabel('amplituda pohybu [mm]')
      mins_ind,peaks_ind= self.plot_extremes(plot=0)
      
      stats=[]
      
      mins=self.ampinter[mins_ind]
      peaks=self.ampinter[peaks_ind]
      
      #compute amplitudes in points of maxima in two ways
      #1 with minimum of minima offset
      #2 with previous minimum offset
      maxabsoffset=peaks-abs(min(mins))      
      maxreloffset=(peaks[1:]-abs(mins[0:len(peaks)-1]))
      #get statistics
      reloffsetstats={'max':max(maxreloffset),'min':min(maxreloffset),'mean':np.mean(maxreloffset)}
      maxoffsetstats={'max':max(maxabsoffset),'min':min(maxabsoffset),'mean':np.mean(maxabsoffset)}     
      
      
      stats.append(reloffsetstats)
      stats.append(maxoffsetstats)
      #store stats in nice format
      for dict_value in stats:
        for k, v in dict_value.items():
            dict_value[k] = round(v, 2)
      
      
      
      plt.plot(self.timeinter[peaks_ind] [0:-1],maxreloffset,color='teal')
      plt.plot(self.timeinter[peaks_ind],maxabsoffset, color='orange')
      ax.legend(['max-min','max-offset'],loc='upper left', bbox_to_anchor=(0.6, 1.00), shadow=True, ncol=2)
      plt.savefig('./graphs/amps'  + str(self.num))
      
      
      
    

    def plot_derivative(self): #plots graph of derivative of signal
          
    
         plt.figure()
         plt.plot(self.timeinter[0:-1],self.derivative, color='firebrick', alpha=0.4 )
         
    def plot_derivative_extremes(self,draw=1): #plots extrema of derivated signal
         plt.figure()
         mins_ind,peaks_ind= self.plot_extremes(ampinter=self.derivative, timeinter= self.timeinter[0:-1],der=1,plot=draw)
         derstats=self.find_min_max_mean(abs(self.derivative))
         
         
         
         
         
    def plot_opclose(self): #plots amplitude graph split to closing and opening
        fig, ax = plt.subplots()
        
        ax.plot(self.timeinter[0:-1],abs(self.opening_velocity), color='blue' )
        ax.plot(self.timeinter[0:-1],abs(self.closing_velocity), color='red' )
        ax.set_xlabel('cas [ms]')
        ax.set_ylabel('rychlost pohybu [mm/ms]')
        ax.legend(['Otváranie','Zatváranie'],loc='upper left', bbox_to_anchor=(0.6, 1.00), shadow=True, ncol=2)
        plt.savefig('./graphs/opclose'  + str(self.num))
       
        
    def plot_opclose_extremes(self): #plots extremes of opening and closing parts of derivated signal
        
        opmin_ind,opmax_ind=self.plot_extremes(ampinter=self.opening_velocity,timeinter=self.timeinter[0:-1],co='orange',der=1)
        clomin_ind,clomax_ind=self.plot_extremes(ampinter=self.closing_velocity,timeinter=self.timeinter[0:-1],co='brown',der=1)
       
        
    def compute_op_close_times(self): #computes durations of each opening an closing of fingers
        
        tmp_list=[]
        tmp_list.append(self.opening_velocity)
        tmp_list.append(self.closing_velocity)
        times={}
        names1=['opening times','closing times']
        names=['opening_vel','closing_vel']
        stats={}
        
        for j,f in enumerate(tmp_list):
            
            tmp=f[np.nonzero(f)]
            stats[names[j]]=self.find_min_max_mean(f[np.nonzero(f)]) 
            f=(f!=0)
            arr=np.asarray(list(self.runs_of_ones(f)))
            arr=(np.add(np.dot(arr,5),5))
            arr=arr[arr>30]            
            stats[names1[j]]=self.find_min_max_mean(arr) 
            
        
    
        pass    
    def compute_acc_mean(self): #computes acceleration and its mean value
        acc=np.diff(self.derivative)
        accstats=self.find_min_max_mean(abs(acc))
        
        plt.figure()
        ax = plt.subplot()
        ax.set_xlabel('cas [ms]')
        ax.set_ylabel("akceleracia pohybu [mm/(s^2)]")
       
       
           
        plt.plot(self.timeinter[0:-2],acc,color='royalblue')
        plt.savefig('./graphs/acc' + str(self.num))
       
        
  
        
        
        

    
    def find_min_max_mean(self,data,freq=0): #find statistics of input
        
        
        # data=np.round(data,2)
        
        if(freq==0):
            stats={'min':min(data),'max':max(data),'mean':np.mean(data)}
        else:
             stats={'min':min(data),'max':max(data),'mean':np.mean(data),'frequency': (1/np.mean(data))*1000}
        
        for k,v in stats.items(): 
            
            v=np.round(v,2)
        
        return stats
    def find_period(self):#find period labeled further down
        fig=plt.figure()
        op_inf_pts=[]
        cl_inf_pts=[]
        
        
        #find period of finger taps      
        
        mins_ind,peaks_ind=self.plot_extremes(plot=0) #get peaks of amplitudes
        
        #method A by computing difference in times of maximal amplitudes
        perA=np.diff(self.timeinter[peaks_ind])
           
        #method B by computing difference in times of minimal amplitudes
        perB=np.diff(self.timeinter[mins_ind])
        
        
        
        #split inflex point to two groups, opening and closing of fingers
        for i,e in enumerate(self.inflex_points[1,:]):
       
         if e in self.cl_vel_time:  
             
             cl_inf_pts.append(self.inflex_points[:,i])
         elif e in self.op_vel_time:
              op_inf_pts.append(self.inflex_points[:,i])
              
        opinf=np.asarray(op_inf_pts)
        clinf=np.asarray(cl_inf_pts)
       
        #method C by computing diference in times of opening inflex points
        perC=np.diff(opinf[:,1])
        #method D by computing difference in times of closing inflex points
        perD=np.diff(clinf[:,1])
         
        #plot periods
        ax = plt.subplot()
        ax.plot(np.arange(len(perA)),perA,color='magenta')
        ax.plot(np.arange(len(perB)),perB,color='steelblue')
        ax.plot(np.arange(len(perC)),perC,color='yellow')
        ax.plot(np.arange(len(perD)),perD,color='green')
        ax.legend(['max','min','inf_open','inf_close'],loc='upper left', bbox_to_anchor=(0.6, 1.00), shadow=True, ncol=2)
        ax.set_xlabel('poradove cislo periody []')
        ax.set_ylabel('trvanie tepu [ms]')
        plt.show()
        
        
        perStats={}
        
        periods=[perA,perB,perC,perD]
        names=['maxamp','minamp','infop','infcl']
        #store in dictionary
        for i,e in enumerate(periods):
            
            perStats[names[i]]=self.find_min_max_mean(e,freq=1)
        
        #save figure for later use
        plt.savefig('./graphs/periods.png')
        pass 
         
        
        
path="./Data/*.txt"
loader =Attribute_finder(path)

loader.plot_extremes(inf=1,plot=1)

loader.plot_derivative_extremes(draw=1)
loader.plot_max_graph()
loader.plot_opclose()
# loader.plot_opclose_extremes()
loader.compute_op_close_times()
loader.compute_acc_mean()

loader.find_period()



# loader.plot_extremes()
# loader.plot_derivative()
# amp=data[:,0]
# time=data[:,1]

# timeinter=np.arange(time[0],time[-1],0.5)
# ampinter=np.interp(timeinter,time,amp)

# plt.plot(timeinter,ampinter, color='red')
# peaks_ind=signal.find_peaks(ampinter,prominence=40, width=50)[0]
# peaks=ampinter[peaks_ind]
# peaks_time=timeinter[peaks_ind]
# mins_ind=signal.find_peaks(np.dot(ampinter,-1), width=50)[0]
# mins=ampinter[mins_ind]
# mins_time=timeinter[mins_ind]

# means1=(peaks-min(mins))/2
# means2=(peaks[1:]-mins[0:len(peaks)-1])/2



# plt.plot(peaks_time,peaks, color='blue', marker='*', linestyle='none')
# plt.plot(mins_time,mins, color='blue', marker='*', linestyle='none')
# plt.plot(peaks_time,means1, color='red', marker='o', linestyle='none')
# plt.plot(peaks_time[0:np.shape(means2)[0]],means2, color='green', marker='o', linestyle='none')



# plt.show()

# filtered=savgol(ampinter,window_length=15,polyorder=5)
# plt.figure(0)
# derivative=np.diff(filtered)
# plt.plot(timeinter[0:-1],derivative, color='blue', alpha=0.4 )

























